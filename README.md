# env.api.telemonitorowanie.pl
https://gitlab.com/softreck/env.api.telemonitorowanie.pl

Środowisko do uruchamiania api.telemonitorowanie.pl
Korzysta ze skryptów do instalacji i zarzadzania uslguami porpzez skrypty basha: apicra.com
+ https://github.com/apicra/debian-python


[![pipeline status](https://gitlab.com/softreck/env.api.telemonitorowanie.pl/badges/master/pipeline.svg)](https://gitlab.com/softreck/env.api.telemonitorowanie.pl/-/commits/master)


## Environment

### Local

linux debian 

    cd debian
    download.sh
    install.sh
    restart.sh
    status.sh
    browser.sh

    
windows 10

    cd win\
    download.bat
    install.bat
    restart.bat
    status.bat
    browser.bat

URL

    http://localhost:8000/
    http://localhost:8000/docs
    http://localhost:8000/docs
        
### Test

linux debian 

    cd debian
    download.sh
    install.sh
    restart.sh
    test.sh
    
windows 10

    cd win\
    download.bat
    install.bat
    restart.bat
    test.bat

URL

    http://test.api.vodapp.com/
    http://test.api.vodapp.com/docs
    http://test.api.vodapp.com/redoc
    
    
### Production

    
linux debian 

    cd debian
    download.sh
    install.sh
    restart.sh
    test.sh
    
windows 10

    cd win\
    download.bat
    install.bat
    restart.bat
    test.bat 


APi URL 
    
    http://api.vodapp.com/
    http://api.vodapp.com/docs
    http://api.vodapp.com/redoc
    

# Github

    ssh-keygen -t rsa -b 4096 -C "email@adress.com"
        
## start the ssh-agent in the background

    eval $(ssh-agent -s)

## Add key
    ssh-add ~/.ssh/id_rsa

## Add to gihub
From Console
    
    clip < ~/.ssh/id_rsa.pub
    cat < ~/.ssh/id_rsa.pub

to Browser github

    https://github.com/settings/ssh/new

to Browser gitlab

    https://gitlab.com/softreck/env.api.telemonitorowanie.pl/-/settings/repository#js-deploy-keys-settings

    
## docs
https://fastapi.tiangolo.com/deployment/


https://github.com/tiangolo/uvicorn-gunicorn-fastapi-docker

## Python

    cd F:\PycharmProjects\env.api.telemonitorowanie.pl\win
    py -m pip install virtualenv
    py -m venv env
    
    .\env\Scripts\activate
    .\env\Scripts\deactivate.bat

    Set-ExecutionPolicy AllSigned

    C:\Windows\py.exe -m venv F:\PycharmProjects\env.api.telemonitorowanie.pl\win\env 
    pip3 install virtualenv
    py -m venv env

    python.install.bat
    python.remove.bat
    python.update.bat

## NGINX 
unix
    
    nginx/install.sh
