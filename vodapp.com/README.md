## Netcup Server

RS 1000 SSD G8 30 per E-Mail schicken.

Leistungen:
Prozessor: 	Intel® Xeon® Gold 6140
Prozessorkerne: 	2 dediziert
Arbeitsspeicher DDR 4 ECC: 	8 GB
Festplatte: 	40 GB SSD
Premium Funktionen:
Fernwartungskonsole: 	JA
DVD-Laufwerk für eigene ISO-Dateien: 	JA
Import von eigenen Images: 	JA
Snapshots (Copy-On-Write): 	JA 



## Runner Installation

wget https://gitlab-runner-downloads.s3.amazonaws.com/latest/deb/gitlab-runner_amd64.deb
apt install ./gitlab-runner_amd64.deb


gitlab-runner --version

	Version:      12.10.1
	Git revision: ce065b93
	Git branch:   12-10-stable
	GO version:   go1.13.8
	Built:        2020-04-22T21:29:52+0000
	OS/Arch:      linux/amd64


gitlab-runner start
gitlab-runner status

gitlab-runner register \
  --non-interactive \
  --url "https://gitlab.com/" \
  --registration-token "" \
  --description "netcup 2CPU 8GB vodapp.com" \
  --executor "shell" \
  --tag-list "vodapp-com"



# After you install and register GitLab runner
sudo apt install docker.io -y

# edit
sudo nano /etc/sudoers

# add the line below
gitlab-runner ALL=(ALL) NOPASSWD: ALL

sudo usermod -aG docker gitlab-runner

sudo gitlab-runner restart


# Verify the Status and check if Docker and Gitlab Runner is enabled on startup:
gitlab-runner status
systemctl is-enabled gitlab-runner
